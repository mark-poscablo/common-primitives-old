import typing
import os

from d3m import container, exceptions, utils as d3m_utils
from d3m.base import utils as base_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives

__all__ = ('ReplaceSemanticTypesPrimitive',)

Inputs = container.DataFrame
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):
    use_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to force primitive to operate on. If any specified column does not have any semantic type from \"from_semantic_types\", it is skipped.",
    )
    exclude_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )
    return_result = hyperparams.Enumeration(
        values=['append', 'replace', 'new'],
        default='replace',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should columns with replaced semantic types be appended, should they replace original columns, or should only columns with replaced semantic types be returned?",
    )
    add_index_columns = hyperparams.UniformBool(
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\".",
    )
    match_logic = hyperparams.Enumeration(
        values=['all', 'any'],
        default='any',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should a column have all of semantic types in \"from_semantic_types\" to have semantic types replaced, or any of them?",
    )
    from_semantic_types = hyperparams.Set(
        elements=hyperparams.Hyperparameter[str](''),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Semantic types to replace. Only columns having semantic types listed here will be operated on, based on \"match_logic\". "
                    "All semantic types listed here will be removed from those columns.",
    )
    to_semantic_types = hyperparams.Set(
        elements=hyperparams.Hyperparameter[str](''),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Semantic types to add to matching columns. All listed semantic types will be added to all columns which had semantic types removed.",
    )


class ReplaceSemanticTypesPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which replaces semantic types with new semantic types for columns in a DataFrame.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '7bae062e-f8b0-4358-91f2-9288a51f3e82',
            'version': '0.2.0',
            'name': "Replace semantic types for columns",
            'python_path': 'd3m.primitives.data_transformation.replace_semantic_types.Common',
            'source': {
                'name': common_primitives.__author__,
                'contact': 'mailto:mitar.commonprimitives@tnode.com',
                'uris': [
                    'https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/replace_semantic_types.py',
                    'https://gitlab.com/datadrivendiscovery/common-primitives.git',
                ],
            },
            'installation': [{
                'type': metadata_base.PrimitiveInstallationType.PIP,
                'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                    git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
                ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        columns_to_use, output_columns = self._produce_columns(inputs, self.hyperparams)

        outputs = base_utils.combine_columns(inputs, columns_to_use, output_columns, return_result=self.hyperparams['return_result'], add_index_columns=self.hyperparams['add_index_columns'])

        return base.CallResult(outputs)

    @classmethod
    def _can_use_column(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int, hyperparams: Hyperparams) -> bool:
        column_metadata = inputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))

        semantic_types = column_metadata.get('semantic_types', [])

        if hyperparams['match_logic'] == 'all':
            return all(semantic_type in semantic_types for semantic_type in hyperparams['from_semantic_types'])
        elif hyperparams['match_logic'] == 'any':
            return any(semantic_type in semantic_types for semantic_type in hyperparams['from_semantic_types'])
        else:
            raise exceptions.UnexpectedValueError("Unknown value of hyper-parameter \"match_logic\": {value}".format(value=hyperparams['match_logic']))

    @classmethod
    def _get_columns(cls, inputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> typing.List[int]:
        def can_use_column(column_index: int) -> bool:
            return cls._can_use_column(inputs_metadata, column_index, hyperparams)

        columns_to_use, columns_not_to_use = base_utils.get_columns_to_use(inputs_metadata, hyperparams['use_columns'], hyperparams['exclude_columns'], can_use_column)

        # We are OK if no columns ended up having semantic types replaced.
        # "base_utils.combine_columns" will throw an error if it cannot work with this.

        if hyperparams['use_columns'] and columns_not_to_use:
            cls.logger.warning("Not all specified columns matches semantic types from \"from_semantic_types\". Skipping columns: %(columns)s", {
                'columns': columns_not_to_use,
            })

        return columns_to_use

    @classmethod
    def _produce_columns(cls, inputs: Inputs, hyperparams: Hyperparams) -> typing.Tuple[typing.List[int], typing.List[Outputs]]:
        columns_to_use = cls._get_columns(inputs.metadata, hyperparams)

        output_columns = []

        for column_index in columns_to_use:
            column = inputs.select_columns([column_index])
            column.metadata = cls._update_metadata(column.metadata, hyperparams)
            output_columns.append(column)

        return columns_to_use, output_columns

    @classmethod
    def _produce_columns_metadata(cls, inputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> typing.Tuple[typing.List[int], typing.List[metadata_base.DataMetadata]]:
        columns_to_use = cls._get_columns(inputs_metadata, hyperparams)

        output_columns = []

        for column_index in columns_to_use:
            column_metadata = inputs_metadata.select_columns([column_index])
            column_metadata = cls._update_metadata(column_metadata, hyperparams)
            output_columns.append(column_metadata)

        return columns_to_use, output_columns

    @classmethod
    def _update_metadata(cls, inputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> metadata_base.DataMetadata:
        inputs_columns_length = inputs_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']

        assert inputs_columns_length == 1, inputs_columns_length

        outputs_metadata = inputs_metadata

        for semantic_type in hyperparams['from_semantic_types']:
            outputs_metadata = outputs_metadata.remove_semantic_type((metadata_base.ALL_ELEMENTS, 0), semantic_type)
        for semantic_type in hyperparams['to_semantic_types']:
            outputs_metadata = outputs_metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 0), semantic_type)

        return outputs_metadata

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]],
                   hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if method_name != 'produce':
            return output_metadata

        if 'inputs' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])

        columns_to_use, output_columns = cls._produce_columns_metadata(inputs_metadata, hyperparams)

        return base_utils.combine_columns_metadata(inputs_metadata, columns_to_use, output_columns, return_result=hyperparams['return_result'], add_index_columns=hyperparams['add_index_columns'])
