import os

from typing import Dict, Tuple
from d3m.container.list import List
from d3m.container.numpy import ndarray
from d3m.metadata import hyperparams
import d3m.metadata.base as metadata_module
from d3m import utils
from d3m.primitive_interfaces.transformer import TransformerPrimitiveBase
from d3m.primitive_interfaces.base import CallResult, DockerContainer

import common_primitives

import numpy as np  # type: ignore
import scipy.ndimage  # type: ignore
import skimage  # type: ignore
import skimage.transform  # type: ignore


Inputs = List
Outputs = ndarray


class Hyperparams(hyperparams.Hyperparams):
    resize_to = hyperparams.Hyperparameter[Tuple[int, int]](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=(0, 0),
        description='Resize all loaded images to this size. Expecting a tuple'
                    ' (H, W) where H is height and W is width. The number of'
                    ' channels (e.g., grayscale, RGB) is not affected. If left'
                    ' as the default value (0, 0), no resizing will be applied.'
    )


class ImageReader(TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    Primitive which takes a list of file names to image files and returns a
    single Numpy array of shape:
        N x C x H x W
    where N is the number of images, C is the number of channels in an image
    (e.g., C = 1 for grayscale, C = 3 for RGB), H is the height, and W is the
    width. All images are expected to be of the same shape and channel
    characteristics.
    """
    __author__ = 'Oxford DARPA D3M Team, Atilim Gunes Baydin <robots.ox.ac.uk>'
    metadata = metadata_module.PrimitiveMetadata({
        'id': '006251ce-4489-411b-9979-07782edb9085',
        'version': '0.1.0',
        'name': 'Image reader',
        'keywords': ['image', 'jpg', 'png', 'tiff'],
        'source': {
            'name': common_primitives.__author__,
            'contact': 'mailto:gunes@robots.ox.ac.uk',
            'uris': [
                'https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/image_reader.py',
                'https://gitlab.com/datadrivendiscovery/common-primitives.git',
            ],
        },
        'installation': [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'
                           .format(git_commit=utils.current_git_commit(os.path.dirname(__file__)))
        }],
        'python_path': 'd3m.primitives.data_preprocessing.image_reader.Common',
        'algorithm_types': ['NUMERICAL_METHOD'],
        'primitive_family': 'DATA_PREPROCESSING',
    })

    def __init__(self, *, hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None) -> None:
        super().__init__(hyperparams=hyperparams,
                         random_seed=random_seed,
                         docker_containers=docker_containers)
        self._resize_to = self.hyperparams['resize_to']

    def produce(self, *,
                inputs: List,
                timeout: float = None,
                iterations: int = None) -> CallResult[Outputs]:
        # TODO consider caching the outputs
        if len(inputs) < 1:
            raise ValueError('Expecting a list of at least one string with file names (received an empty list).')
        images = []
        image_shape = None
        for file_name in inputs:
            img = scipy.ndimage.imread(file_name)
            if self._resize_to[0] != 0 or self._resize_to[1] != 0:
                img = skimage.transform.resize(img, self._resize_to)
            else:
                if image_shape is None:
                    image_shape = img.shape
                elif image_shape != img.shape:
                    raise ValueError('Expecting all images to be of the same shape {} (received {}).'.format(image_shape, img.shape))
            if img.ndim == 2:
                img = np.expand_dims(img, axis=0)
            elif img.ndim == 3:
                img = img.transpose((2, 0, 1))
            else:
                raise ValueError('Unsupported image.')
            images.append(img)

        return CallResult(np.stack(images))
