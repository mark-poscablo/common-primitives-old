import unittest
import os
from urllib import request
import numpy as np
import sys
from typing import Dict
#from common_primitives import image_reader, image_featurize
import tempfile


@unittest.skip
class ImageClassifyTestCase(unittest.TestCase):

    MODELS = "https://github.com/fchollet/deep-learning-models/releases/download/v0.2/resnet50_weights_tf_dim_ordering_tf_kernels.h5"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.download_resnet50()

    @classmethod
    def download_resnet50(cls):
        with tempfile.TemporaryDirectory() as tmpdirname:
            cls.tmp_dir = tmpdirname
            fname = os.path.join(tmpdirname, os.path.basename(cls.MODELS))
            if not os.path.isfile(fname):
                request.urlretrieve(cls.MODELS, fname)

    def test_image_featurize(self):
        ir = image_reader.ImageReader(
                    hyperparams=image_reader.Hyperparams(
                                        image_reader.Hyperparams.defaults(),
                                        resize_to=(224,224)))

        test_dir = os.path.dirname(os.path.realpath(__file__)) + "/test_image"
        train_files = ['cifar10_bird_1.png',
                       'cifar10_bird_2.png',
                       'cifar10_bird_3.png',
                       'cifar10_bird_4.png',
                       'cifar10_bird_5.png',
                       'cifar10_ship_1.png',
                       'cifar10_ship_2.png',
                       'cifar10_ship_3.png',
                       'cifar10_ship_4.png',
                       'cifar10_ship_5.png']
        train_files = ['{}/{}'.format(test_dir, i) for i in train_files]

        train_inputs = ir.produce(inputs=train_files).value
        train_outputs = np.array([0,0,0,0,0,1,1,1,1,1])

        hy = image_featurize.Hyperparams.defaults()
        vol = dict()
        fname = os.path.join(self.tmp_dir, os.path.basename(self.MODELS))
        vol['resnet50'] = fname
        clf = image_featurize.ImageTransferLearningTransformer(hyperparams=hy, volumes=vol)

        featurized_outputs = clf.produce(inputs=train_inputs).value

        from d3m.container import DataFrame as d3m_dataframe
        from d3m.primitive_interfaces.base import CallResult
        outputs = d3m_dataframe

        assert isinstance(featurized_outputs, outputs)

    def test_cifar10_accuracy(self):
        ir = image_reader.ImageReader(
                    hyperparams=image_reader.Hyperparams(
                                    image_reader.Hyperparams.defaults(),
                                    resize_to=(224,224)))

        test_dir = os.path.dirname(os.path.realpath(__file__)) + "/test_image"
        train_files = ['cifar10_bird_1.png',
                       'cifar10_bird_2.png',
                       'cifar10_bird_3.png',
                       'cifar10_bird_4.png',
                       'cifar10_bird_5.png',
                       'cifar10_ship_1.png',
                       'cifar10_ship_2.png',
                       'cifar10_ship_3.png',
                       'cifar10_ship_4.png',
                       'cifar10_ship_5.png']
        train_files = ['{}/{}'.format(test_dir, i) for i in train_files]

        train_inputs = ir.produce(inputs=train_files).value
        train_outputs = np.array([0,0,0,0,0,1,1,1,1,1])

        hy = image_featurize.Hyperparams.defaults()
        vol = dict()
        fname = os.path.join(self.tmp_dir, os.path.basename(self.MODELS))
        vol['resnet50'] = fname
        clf = image_featurize.ImageTransferLearningTransformer(hyperparams=hy, volumes=vol)

        featurized_outputs = clf.produce(inputs=train_inputs).value

        from sklearn.svm import SVC
        clf = SVC()
        clf.fit(featurized_outputs, train_outputs)
        outputs = clf.predict(featurized_outputs)

        accuracy = np.sum(outputs == train_outputs) / len(outputs)
        self.assertTrue(accuracy >= 0.5)


if __name__ == '__main__':
    unittest.main()
