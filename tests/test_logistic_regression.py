import unittest

import numpy as np

#from common_primitives.logistic_regression import BayesianLogisticRegression, Hyperparams


@unittest.skip
class LogisticRegressionTestCase(unittest.TestCase):
    def test_fit_sample_produce(self):
        a = BayesianLogisticRegression(hyperparams=Hyperparams.defaults())
        num_features = 3
        num_rows = 10

        train_x = np.random.randn(num_rows, num_features)
        train_y = np.random.binomial(1, 0.5, num_rows)

        a.set_training_data(inputs=train_x, outputs=train_y)

        # this takes a few seconds
        a.fit()
        # just test that it gives right types
        test_x = np.random.randn(5, num_features)
        assert (type(a.log_likelihoods(inputs=train_x, outputs=train_y).value[0]) == np.float64)
        assert (type(a.produce(inputs=test_x).value[0]) == int or
                type(a.produce(inputs=test_x).value[0]) == np.int64)
        assert (type(a.sample(inputs=test_x, num_samples=7).value[0][0]) == int or
                type(a.sample(inputs=test_x, num_samples=7).value[0][0]) == np.int64)
        assert np.array_equal(a.sample(inputs=test_x, num_samples=2).value.shape, (2, 5))


if __name__ == '__main__':
    unittest.main()
