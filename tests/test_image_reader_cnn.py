import unittest
import os
import numpy as np
from common_primitives import image_reader, convolutional_neural_net


@unittest.skip
class ImageReaderCNNTestCase(unittest.TestCase):
    def test_mnist_accuracy_lenet(self):
        ir = image_reader.ImageReader(hyperparams=image_reader.Hyperparams.defaults())

        test_dir = os.path.dirname(os.path.realpath(__file__)) + "/test_image"
        train_files = ['mnist_0_1.png',
                       'mnist_0_2.png',
                       'mnist_0_3.png',
                       'mnist_0_4.png',
                       'mnist_0_5.png',
                       'mnist_1_1.png',
                       'mnist_1_2.png',
                       'mnist_1_3.png',
                       'mnist_1_4.png',
                       'mnist_1_5.png']
        train_files = ['{}/{}'.format(test_dir, i) for i in train_files]

        train_inputs = ir.produce(inputs=train_files).value
        train_outputs = np.array([0,0,0,0,0,1,1,1,1,1])

        hy = convolutional_neural_net.Hyperparams(convolutional_neural_net.Hyperparams.defaults(), family='lenet', output_dim=2, loss_type='crossentropy', fit_threshold=1e-3)
        cnn = convolutional_neural_net.ConvolutionalNeuralNet(hyperparams=hy)

        cnn.set_training_data(inputs=train_inputs, outputs=train_outputs)
        cnn.fit(iterations=1000)
        outputs = cnn.produce(inputs=train_inputs).value
        outputs = np.argmax(outputs, 1)
        accuracy = np.sum(outputs == train_outputs) / len(outputs)
        self.assertTrue(accuracy >= 0.9)

    def test_mnist_accuracy_alexnet(self):
        ir = image_reader.ImageReader(hyperparams=image_reader.Hyperparams(image_reader.Hyperparams.defaults(), resize_to=(256,256)))

        test_dir = os.path.dirname(os.path.realpath(__file__)) + "/test_image"
        train_files = ['mnist_0_1.png',
                       'mnist_0_2.png',
                       'mnist_0_3.png',
                       'mnist_0_4.png',
                       'mnist_0_5.png',
                       'mnist_1_1.png',
                       'mnist_1_2.png',
                       'mnist_1_3.png',
                       'mnist_1_4.png',
                       'mnist_1_5.png']
        train_files = ['{}/{}'.format(test_dir, i) for i in train_files]

        train_inputs = ir.produce(inputs=train_files).value
        train_outputs = np.array([0,0,0,0,0,1,1,1,1,1])

        hy = convolutional_neural_net.Hyperparams(convolutional_neural_net.Hyperparams.defaults(), family='alexnet', output_dim=2, loss_type='crossentropy', fit_threshold=1e-3)
        cnn = convolutional_neural_net.ConvolutionalNeuralNet(hyperparams=hy)

        cnn.set_training_data(inputs=train_inputs, outputs=train_outputs)
        cnn.fit(iterations=10)
        outputs = cnn.produce(inputs=train_inputs).value
        outputs = np.argmax(outputs, 1)
        accuracy = np.sum(outputs == train_outputs) / len(outputs)
        self.assertTrue(accuracy >= 0.1) # Accuracy threshold kept low for decreasing the test running time

    def test_cifar10_accuracy(self):
        ir = image_reader.ImageReader(hyperparams=image_reader.Hyperparams.defaults())

        test_dir = os.path.dirname(os.path.realpath(__file__)) + "/test_image"
        train_files = ['cifar10_bird_1.png',
                       'cifar10_bird_2.png',
                       'cifar10_bird_3.png',
                       'cifar10_bird_4.png',
                       'cifar10_bird_5.png',
                       'cifar10_ship_1.png',
                       'cifar10_ship_2.png',
                       'cifar10_ship_3.png',
                       'cifar10_ship_4.png',
                       'cifar10_ship_5.png']
        train_files = ['{}/{}'.format(test_dir, i) for i in train_files]

        train_inputs = ir.produce(inputs=train_files).value
        train_outputs = np.array([0,0,0,0,0,1,1,1,1,1])

        hy = convolutional_neural_net.Hyperparams(convolutional_neural_net.Hyperparams.defaults(), family='lenet', output_dim=2, loss_type='crossentropy', fit_threshold=1e-3)
        cnn = convolutional_neural_net.ConvolutionalNeuralNet(hyperparams=hy)

        cnn.set_training_data(inputs=train_inputs, outputs=train_outputs)
        cnn.fit(iterations=1000)
        outputs = cnn.produce(inputs=train_inputs).value
        outputs = np.argmax(outputs, 1)
        accuracy = np.sum(outputs == train_outputs) / len(outputs)
        self.assertTrue(accuracy >= 0.9)

if __name__ == '__main__':
    unittest.main()
