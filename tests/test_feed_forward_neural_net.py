import os
import pickle
import unittest

from d3m import container
from d3m.metadata import base as metadata_base

from common_primitives import dataset_to_dataframe, extract_columns_semantic_types, random_forest, utils, column_parser


class FeedForwardNeuralNetTestCase(unittest.TestCase):
    @unittest.skip("Skipping regression test - FFNN reworked for classification only")
    def test_regression_xor(self):
        """
        MLP train (fit) and test (produce) expected resuts with XOR
        """
        # These particular values are picked because they result in a test that concludes quickly
        hy = Hyperparams(Hyperparams.defaults(), depth = 4, width = 64, activation_type = 'tanh', last_activation_type = 'sigmoid', fit_threshold=1e-3)
        primitive = FeedForwardNeuralNet(hyperparams=hy)
        xor_inputs = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
        xor_outputs = np.array([[0], [1], [1], [0]])
        primitive.set_training_data(inputs=xor_inputs, outputs=xor_outputs)
        primitive.fit(iterations=10000)
        outputs = primitive.produce(inputs=xor_inputs).value
        self.assertTrue(np.allclose(outputs, xor_outputs, 0.1, 0.1))

    @unittest.skip("Skipping classification test - need to update MNIST test with metadata under new primitive")
    def test_classification_mnist(self):
        """
        MLP train (fit) and test (produce) expected resuts with MNIST
        """
        hy = Hyperparams(Hyperparams.defaults(), depth = 4, width = 64, activation_type = 'relu', loss_type = 'crossentropy', fit_threshold=1e-3, output_dim = 10)
        primitive = FeedForwardNeuralNet(hyperparams=hy)
        mnist_train_inputs, mnist_train_outputs = get_mnist_train_data(10)
        primitive.set_training_data(inputs=mnist_train_inputs, outputs=mnist_train_outputs)
        primitive.fit(iterations=10000)
        outputs = primitive.produce(inputs=mnist_train_inputs).value
        outputs = np.argmax(outputs, 1)
        accuracy = np.sum(outputs == mnist_train_outputs) / len(outputs)
        self.assertTrue(accuracy >= 0.9)

    @unittest.skip("Skipping params setting for temporarily - TODO put back when ready")
    def test_get_set_params(self):
        """
        MLP get and set params
        """
        hy = Hyperparams(Hyperparams.defaults(), depth = 4, width = 64, activation_type = 'tanh')
        primitive = FeedForwardNeuralNet(hyperparams=hy)
        xor_inputs = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
        xor_outputs = np.array([[0], [1], [1], [0]])
        primitive.set_training_data(inputs=xor_inputs, outputs=xor_outputs)
        params = primitive.get_params()
        primitive.set_params(params=params)
        self.assertTrue(True)


if __name__ == '__main__':
    unittest.main()
