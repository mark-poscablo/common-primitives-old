import unittest

import numpy as np

from common_primitives.convolutional_neural_net import ConvolutionalNeuralNet, Hyperparams
from utils import get_mnist_train_data
import skimage.transform


@unittest.skip
class ConvolutionalNeuralNetTestCase(unittest.TestCase):
    def test_mnist_accuracy_lenet(self):
        """
        CNN train (fit) and test (produce) expected resuts with a subset of mnist
        """
        hy = Hyperparams(Hyperparams.defaults(), family='lenet', output_dim=10, loss_type='crossentropy', fit_threshold=1e-3)
        primitive = ConvolutionalNeuralNet(hyperparams=hy)
        mnist_train_inputs, mnist_train_outputs = get_mnist_train_data(10)
        primitive.set_training_data(inputs=mnist_train_inputs, outputs=mnist_train_outputs)
        primitive.fit(iterations=1000)
        outputs = primitive.produce(inputs=mnist_train_inputs).value
        outputs = np.argmax(outputs, 1)
        accuracy = np.sum(outputs == mnist_train_outputs) / len(outputs)
        self.assertTrue(accuracy >= 0.9)
