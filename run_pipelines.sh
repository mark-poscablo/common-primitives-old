#!/bin/bash

mkdir -p results

overall_result="0"

while IFS= read -r meta_file; do
  run_id="$(basename -s .meta "$meta_file")"

  if [[ -e "results/$run_id" ]]; then
    echo ">>> Skipping '$meta_file'."
    continue
  else
    mkdir -p "results/$run_id"
  fi

  json_file="${meta_file%.meta}.json"
  yml_file="${meta_file%.meta}.yml"
  pipelines_path="$(dirname "$meta_file")"

  if [[ -e "$json_file" ]]; then
    pipeline_file="$json_file"
  elif [[ -e "$yml_file" ]]; then
    pipeline_file="$yml_file"
  else
    echo ">>> ERROR: Could not find pipeline file for '$meta_file'."
    overall_result="1"
    continue
  fi

  echo ">>> Running '$meta_file'."
  python3 -m d3m --pipelines-path "$pipelines_path" \
    --strict-resolving \
    runtime \
    --datasets /data/datasets --volumes /data/static_files \
    fit-score --meta "$meta_file" --pipeline "$pipeline_file" \
    --output "results/$run_id/predictions.csv" \
    --scores "results/$run_id/scores.csv" \
    --output-run "results/$run_id/pipeline_runs.yml"
  result="$?"

  if [[ "$result" -eq 0 ]]; then
    echo ">>> SUCCESS"
  else
    echo ">>> ERROR"
    overall_result="1"
  fi
done < <(find pipelines -name '*.meta')

exit "$overall_result"
